This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Flickr Photos

![alt text](http://i.piccy.info/i9/48b0933aa8bdbabe0351733c6abb4c7a/1545240418/70142/1289884/Screenshot.jpg)

### `git clone https://gitlab.com/andrew.velkov/flickr.git`

### `cd flickr`

### `yarn install`

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### `yarn build`