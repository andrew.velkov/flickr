import React from 'react';
import PropTypes from 'prop-types';

import styles from '../../../styles/components/PhotoList.module.scss';

const PhotoListComponent = ({ status, photos, loaded }) => (
  <React.Fragment>
    {status && <ul className={styles.photoList}>
      {(loaded && photos.photo.length > 0) && photos.photo.map(photo => {
        return (
          <li key={photo.id} className={styles.photoList__item}>
            <img src={`https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}_z.jpg`} alt="" />
          </li>
        );
      })}
    </ul>}

    {(!status || !loaded || photos.photo.length === 0) && <div className={styles.well}>
      {loaded && <p className={styles.well__text}>No results...</p>}
    </div>}
  </React.Fragment>
);

PhotoListComponent.propTypes = {
  loaded: PropTypes.bool,
  loading: PropTypes.bool,
};

export default PhotoListComponent;
