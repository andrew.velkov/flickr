import React from 'react';
import PropTypes from 'prop-types';
import ReactPaginate from 'react-paginate';

import styles from '../../../styles/components/Pagination.module.scss';

const Pagination = ({ pageCount, handleSetPage }) => (
  <section className={ styles.pagination }>
    <div className={ styles.pagination__wrap }>
      <div className={ styles.pagination__item }>
        <ReactPaginate
          previousLabel={ 'prev ' }
          nextLabel={ 'next' }
          breakLabel={ '...' }
          pageCount={ pageCount }
          marginPagesDisplayed={ 3 }
          pageRangeDisplayed={ 2 }
          onPageChange={ ({ selected }) => handleSetPage(selected + 1) }
        />
      </div>
    </div>
  </section>
);

Pagination.propTypes = {
  handleSetPage: PropTypes.func,
  pageCount: PropTypes.number,
};

export default Pagination;
