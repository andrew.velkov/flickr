import React from 'react';
import PropTypes from 'prop-types';
import { TextField } from '@material-ui/core';

import styles from '../../../styles/components/InputSearch.module.scss';

const InputSearchComponent = ({ value, onChange, onClick }) => (
  <div className={styles.search}>
    <TextField
      label="Search photos..."
      name="search"
      type="search"
      margin="normal"
      variant="filled"
      fullWidth
      placeholder='e.g: 劇場版 七つの大罪 天空の囚われ人.'
      value={value}
      onChange={ onChange }
    />
    <button className={styles.search__button} onClick={onClick}>Go</button>
  </div>
);

InputSearchComponent.propTypes = {
  onChange: PropTypes.func,
};

export default InputSearchComponent;
