import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import PhotoSearchPage from '../pages/PhotoSearchPage';
import NotFoundPage from '../pages/NotFoundPage';

import history from './history';

export default class Routes extends Component {
  render() {
    return (
      <Router history={ history }>
        <Switch>
          <Route exact path='/' render={ (props) => <PhotoSearchPage {...props} /> } />
          <Route component={ NotFoundPage } />
        </Switch>
      </Router>
    );
  }
}
