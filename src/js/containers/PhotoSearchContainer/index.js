import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { getPhotos } from '../../actions';
import InputSearchComponent from '../../components/InputSearchComponent';
import PhotoListComponent from '../../components/PhotoListComponent';
import Pagination from '../../components/Pagination';
import Fetching from '../../components/Fetching';

class PhotoSearchContainer extends Component {
  static propTypes = {
    photos: PropTypes.object,
    getPhotos: PropTypes.func,
  };

  state = {
    selectedPage: 1,
    search: 'cats',
  }

  componentDidMount() {
    const { search } = this.state;

    this.props.getPhotos(search);
  }

  setPagination = (selectedPage) => {
    const { search } = this.state;

    this.props.getPhotos(search, selectedPage);
    this.setState({
      selectedPage,
    });
  };

  handleChange = (e) => {
     this.setState({
      [e.target.name]: e.target.value,
    });
  }

  searchHandler = (e) => {
    e.preventDefault();
    const { search } = this.state;

    this.props.getPhotos(search);
  }

  render() {
    const { data: { photos, stat }, loaded, loading } = this.props.photos;
    const { search } = this.state;
    const isPhotos = photos !== undefined;
    const isStat = stat === 'ok';

    return (
      <React.Fragment>
        <InputSearchComponent
          value={search}
          onChange={this.handleChange}
          onClick={this.searchHandler}
        />

        <Fetching isFetching={loading}>
          <PhotoListComponent
            status={isStat}
            photos={photos}
            loaded={loaded}
          />
        </Fetching>

        {(isPhotos && photos.photo.length > 0) && <Pagination
          pageCount={photos.pages / photos.perpage}
          handleSetPage={this.setPagination}
        />}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    photos: state.get.photos,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getPhotos: (search, page) => dispatch(getPhotos(search, page)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PhotoSearchContainer);
