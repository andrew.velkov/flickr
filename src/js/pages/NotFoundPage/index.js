import React from 'react';
import { Link } from 'react-router-dom';

import styles from '../../../styles/pages/NotFound.module.scss';

const logo = require('../../../assets/images/logo.svg');

const NotFoundPage = () => (
  <section className={styles.notFound}>
    <article className={styles.notFound__inner}>
      <img className={styles.notFound__img} src={ logo } alt="Page not found" />
      <h2>Page not found! <Link to="/"> Back to home</Link></h2>
    </article>
  </section>
);

export default NotFoundPage;
