import React from 'react';
import PhotoSearchContainer from '../../containers/PhotoSearchContainer';

const PhotoSearchPage = () => (
  <PhotoSearchContainer />
);

export default PhotoSearchPage;
