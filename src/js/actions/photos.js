import * as type from '../constants/photos';
import flickr from '../config/flickr';

export const getPhotos = (search = '', page = 1, perPage = 10) => {
  return {
    types: [type.GET_PHOTOS, type.GET_PHOTOS_SUCCESS, type.GET_PHOTOS_ERROR],
    request: {
      method: 'GET',
      url: `${flickr.base_url}/?method=flickr.photos.search&api_key=${flickr.api_key}&text=${search}&per_page=${perPage}&page=${page}&format=json&nojsoncallback=1`,
    },
  };
};
