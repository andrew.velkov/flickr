export const GET_PHOTOS = 'flickr/photos/LOAD';
export const GET_PHOTOS_SUCCESS = 'flickr/photos/LOAD_SUCCESS';
export const GET_PHOTOS_ERROR = 'flickr/photos/LOAD_ERROR';
