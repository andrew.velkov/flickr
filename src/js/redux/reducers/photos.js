const initialState = {
  loaded: false,
  loading: false,
  data: [],
};

export default function photos(params = '') {
  return (state = initialState, action = {}) => {
    switch (action.type) {
      case `flickr/${params}/LOAD`:
        return {
          ...state,
          loading: true,
          loaded: false,
        };
      case `flickr/${params}/LOAD_SUCCESS`:
        return {
          ...state,
          loading: false,
          loaded: true,
          data: action.payload,
        };
      case `flickr/${params}/LOAD_ERROR`:
        return {
          ...state,
          loading: false,
          loaded: false,
        };
      default:
        return state;
    }
  };
};