import { combineReducers } from 'redux';

import photos from './photos';

const reducers = combineReducers({
  get: combineReducers({
    photos: photos('photos'),
  }),
});

export default reducers;
